plugins {
    id 'java-library'
    id 'maven-publish'
    id 'com.jfrog.bintray' version '1.8.0'
}

repositories {
    jcenter()
}

dependencies {
    api 'org.slf4j:slf4j-api:1.7.25'
    implementation 'org.apache.commons:commons-lang3:3.5' 
    implementation 'commons-io:commons-io:2.4'
    implementation 'org.apache.commons:commons-collections4:4.1'
    implementation 'com.fasterxml.jackson.core:jackson-databind:2.8.8.1'
    implementation 'com.fasterxml.jackson.module:jackson-module-parameter-names:2.8.8'
    implementation 'com.fasterxml.jackson.datatype:jackson-datatype-jdk8:2.8.8'
    implementation 'com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.8.8'
    implementation 'org.glassfish.jersey.core:jersey-client:2.25.1'
    implementation 'com.fasterxml.jackson.jaxrs:jackson-jaxrs-json-provider:2.8.8'
    testImplementation 'org.testng:testng:6.10'
}

test {
    useTestNG()
}

task sourcesJar(type: Jar) {
    classifier = 'sources'
    from sourceSets.main.allSource
}

task javadocJar(type: Jar) {
    classifier = 'javadoc'
    from javadoc.destinationDir
}

ext.getGitVersion = {
    def out = new ByteArrayOutputStream()
    if (exec {
        commandLine 'git', 'describe', '--long', '--dirty'
        standardOutput = out
        errorOutput = new ByteArrayOutputStream()
        ignoreExitValue = true 
    }.getExitValue() != 0) {
        return null
    }
    def matcher = 
        out.toString().trim() =~ 
        /v([0-9]+)\.([0-9]+)\.([0-9]+)-([0-9]+)-([^\-]+)(?:-(dirty))?/
    if (matcher.matches()) {
        def majorVersion = Integer.parseInt(matcher.group(1))
        def minorVersion = Integer.parseInt(matcher.group(2))
        def patchVersion = Integer.parseInt(matcher.group(3))
        def numberOfCommits = Integer.parseInt(matcher.group(4))
        def commitHash = matcher.group(5)
        def dirty = Objects.equals(matcher.group(6), 'dirty') 
        if (numberOfCommits == 0 && !dirty) {
            majorVersion + '.' + minorVersion + '.' + patchVersion
        }
        else {
            majorVersion + '.' + minorVersion + '.' + ( patchVersion + 1 ) +
            '-adhoc.' + numberOfCommits + (dirty ? '.dirty' : '') + '+' +
            commitHash 
        }
    }
    else {
        null
    }
}

ext.isBaselineGitVersion = {
    def out = new ByteArrayOutputStream()
    if (exec {
        commandLine 'git', 'describe', '--long', '--dirty'
        standardOutput = out
        errorOutput = new ByteArrayOutputStream()
        ignoreExitValue true
    }.getExitValue() != 0) {
      return false
    }
    def matcher = 
        out.toString().trim() =~ /v.*?-([0-9]+)-[^\-]+(?:-(dirty))?/
    if (matcher.matches()) {
        def numberOfCommits = Integer.parseInt(matcher.group(1))
        def dirty = Objects.equals(matcher.group(2), 'dirty') 
        numberOfCommits == 0 && !dirty
    }
    else {
        false
    }
}

version = getGitVersion()
group = 'org.thatscloud'

task version {
    doLast {
        println getVersion()
        if (isBaselineGitVersion()) {
            println 'This is a baseline (releasable) version.'
        }
        else {
            println 'This is a non-baseline (non-releasable) version.'
        }
    }
}

bintrayUpload {
  doFirst {
      if (!isBaselineGitVersion()) {
          throw new GradleException( 
              'Must be a baseline git version to be relased to bintray. ' +
              'Run the \'version\' task for more information.')
      }
      property('bintrayUser')
      property('bintrayKey')
      property('sonatypeOssUser')
      property('sonatypeOssPassword')
  }
}

bintray {
    publish = true
    user = findProperty('bintrayUser')
    key = findProperty('bintrayKey')
    publications = ['mavenJava'] 
    pkg {
        repo = 'maven'
        name = 'pubj'
        userOrg = 'thatscloud'
        licenses = ['MIT']
        vcsUrl = 'https://gitlab.com/thatscloud/pubj'
        version {
            gpg {
                sign = true
            }
            name = getVersion()
            desc = 'pubj ' + getVersion()
            released  = new Date()
            vcsTag = 'v' + getVersion()
            mavenCentralSync {
                user = findProperty('sonatypeOssUser')
                password = findProperty('sonatypeOssPassword')
            }
        }
    }
}

publishing {
    publications {
        mavenJava(MavenPublication) {
            from components.java
            artifact sourcesJar
            artifact javadocJar
            pom.withXml {
                asNode().children().last() + { 
                    // See http://gradle.1045684.n5.nabble.com/New-maven-publishing-featureset-tp5711110p5711311.html 
                    // and http://groovy-lang.org/closures.html#_delegation_strategy_2
                    resolveStrategy = Closure.DELEGATE_FIRST 
                    description 'pubj'
                    name 'pubj'
                    url 'https://gitlab.com/thatscloud/pubj'
                    licenses {
                        license {
                            name 'The MIT License'
                            url 'https://opensource.org/licenses/MIT'
                            distribution 'repo'
                        }
                    }
                    developers {
                        developer {
                            id "oss"
                            name "OSS Developers"
                            email "oss@thatscloud.org"
                        }
                    }
                    
                    scm {
                       url 'https://gitlab.com/thatscloud/pubj'
                    }
                }
            }
        }
    }
}

task copyRuntimeLibs(type: Copy) {
  into "$buildDir/dependency-libs"
  from configurations.runtimeClasspath
}

